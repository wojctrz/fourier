#include <cmath>
#include <numbers>

#include "slowFourierTransform.hpp"

std::vector< std::complex<double> > makeSlowFourierTransorfm(std::vector<double>& input)
{
    using namespace std::complex_literals;
    using namespace std::numbers;

    const int numSamples = input.size();
    std::vector< std::complex<double> > result(numSamples);
    for(int k = 0; k < numSamples; ++k)
    {

        std::complex<double> sum = 0;
        std::complex<double> omega = std::exp(1i * ((2.0 * pi) / numSamples));
        for(int n = 0; n < numSamples; ++n)
        {
            sum += input[n] * std::pow(omega, -k*n);
        }

        result[k] = sum;
    }

    return result;
}
