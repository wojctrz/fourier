#pragma once

#include <vector>
#include <complex>

// With Cooley-Tukey (Radix-2)  algorithm!
std::vector< std::complex<double> > makeFastFourierTransorfm(std::vector<double>& input);

void addZeroPadding(std::vector<double>& input);

size_t findNextPowOf2(size_t dataSize);

void divideInputVector(std::vector<double>& input, std::vector<double>& odd, std::vector<double>& even);

std::vector< std::complex<double> > makeFastFourierTransorfm(std::vector<double>& input);