#include <iostream>
#include <iomanip>
#include <vector>
#include <random>
#include <chrono>

#include "reader.hpp"
#include "slowFourierTransform.hpp"
#include "fastFourierTransform.hpp"

std::vector<double> generateRandomData(size_t size)
{
    std::vector<double> vec(size);

    std::random_device rd;
    std::default_random_engine eng(rd());
    std::uniform_real_distribution<double> distr(-10, 10);

    for(size_t i = 0; i < size; ++i)
    {
        vec[i] = distr(eng);
    }

    return vec;
}

void measureExecutionTime(std::vector< std::complex<double> > (*fooPointer)(std::vector<double>&), std::vector<double>& input, const char *desc)
{
    std::cout << "measuring time of " << desc << std::endl;

    auto std_start = std::chrono::high_resolution_clock::now();
    auto res = fooPointer(input);
    auto std_end = std::chrono::high_resolution_clock::now();

    double std_time_elapsed = std::chrono::duration_cast<std::chrono::microseconds>(std_end - std_start).count();

    std::cout << "Time elapsed is " << std_time_elapsed << " microseconds\n";
}

int main()
{
    // auto input = readDataFromFile("dupa.txt");

    auto input = generateRandomData(1024);

    measureExecutionTime(makeSlowFourierTransorfm, input, "Slow Fourier transform");
    measureExecutionTime(makeFastFourierTransorfm, input, "Fast Fourier transform");

}
