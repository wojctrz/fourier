#include "fastFourierTransform.hpp"
#include <cmath>
#include <numbers>

void addZeroPadding(std::vector<double>& input)
{
    auto oldVecSize = input.size();
    auto reshapedVecSize = findNextPowOf2(oldVecSize);

    input.resize(reshapedVecSize, 0);
}

size_t findNextPowOf2(size_t dataSize)
{
    return pow(2, ceil(log(dataSize)/log(2)));
}

void divideInputVector(std::vector<double>& input, std::vector<double>& odd, std::vector<double>& even)
{
    for(size_t i = 0; i < input.size(); i+=2)
    {
        // input vector size shall be always even, so this (i+=2) is ok
        even.push_back(input[i]);
        odd.push_back(input[i+1]);
    }
}

std::vector< std::complex<double> > makeFastFourierTransorfm(std::vector<double>& input)
{
    using namespace std::complex_literals;
    using namespace std::numbers;

    addZeroPadding(input); // TODO: Move it to make it executed only once per whole algorithm

    auto numSamples = input.size();

    if(numSamples == 1)
    {
        return {std::complex<double>(input[0], 0)};
    }

    std::vector<double> oddInput;
    std::vector<double> evenInput;
    divideInputVector(input, oddInput, evenInput);
    std::vector< std::complex<double> > oddFft = makeFastFourierTransorfm(oddInput);
    std::vector< std::complex<double> > evenFft = makeFastFourierTransorfm(evenInput);

    std::vector< std::complex<double> > result(numSamples);
    for(size_t i = 0; i <= ((numSamples/2) - 1); ++i)
    {
        auto p = evenFft[i];
        auto q = std::exp(static_cast<double>(i) * 1i * ((-2.0 * pi) / numSamples)) * oddFft[i];
        result[i] = p + q;
        result[i + numSamples/2] = p - q; 
    }

    return result;
}