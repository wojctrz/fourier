#include "reader.hpp"
#include <fstream>
#include <iostream>

std::vector<double> readDataFromFile(std::string path)
{
    std::fstream fileHandle(path);

    if(fileHandle.is_open())
    {
        std::vector<double> vec;
        double justAnotherNextVal;
        while(fileHandle >> justAnotherNextVal)
        {
            vec.push_back(justAnotherNextVal);
        }
        return vec;
    }
    // TODO: some error handling??
}